#!/bin/bash

# Where are we?
pwd=$(pwd)

# Find out what the src and dest are...
echo "Please enter your source path"
read -r src
echo "Please enter your destination path"
read -r dest

# Check for the 'bin' dir in the users 'Home' directory and create it if not present...
if [ -d "$HOME"/bin ];
  then echo "$HOME/bin exists, nothing to do..."
    else echo "$HOME/bin not found - creating..." && mkdir "$HOME"/bin && echo "$HOME/bin created!"
fi

# Make the root dir...
mkdir "$HOME"/bin/backup_home

# Copy files over to ~/bin
cp "$pwd"/Backup.sh "$HOME"/bin/backup_home
cp "$pwd"/Backup.svg "$HOME"/bin/backup_home
cp "$pwd"/LICENSE "$HOME"/bin/backup_home
cp "$pwd"/README.md "$HOME"/bin/backup_home
cp "$pwd"/uninstall.sh "$HOME"/bin/backup_home
cp "$pwd"/Backup.desktop "$HOME"/.local/share/applications

# Set source and destination in Backup.sh
sed -i "s|source=|source=$src|" "$HOME"/bin/backup_home/Backup.sh
sed -i "s|destination=|destination=$dest|" "$HOME"/bin/backup_home/Backup.sh


# Edit the menu entry file...
sed -i "s|Exec=|Exec=$HOME/bin/backup_home/Backup.sh|" "$HOME"/.local/share/applications/Backup.desktop
sed -i "s|Icon=|Icon=$HOME/bin/backup_home/Backup.svg|" "$HOME"/.local/share/applications/Backup.desktop

# Let the user know we have finished...
echo "All done! Press 'Enter' to close this window..."
read -r