#!/bin/bash

# Uninstall the app...
echo "Removing all files and shortcuts..."

rm -r "$HOME"/bin/backup_home
rm "$HOME"/.local/share/applications/Backup.desktop

echo "Application now removed!"
echo "Press 'Enter' to close this window..."
read -r