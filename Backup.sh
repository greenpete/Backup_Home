#!/bin/bash

##################################
# Title: 2nd gen' backup script  #
# Author: Peter Green            #
# Version: 0.5                   #
# Date: 15/06/2019               #
# Last modified: 18/01/2023      #
##################################

# Vars...
source=
destination=

# Start the timer.
res1=$(date +%s.%N)

# Ask if the user would like to backup the three main things this script is designed to back up.
echo "
        ############################################
        #                                          #
        #  Would you like to backup Firefox? y/n   #
        #                                          #
        ############################################"
read -r ff

until [ "$ff" = y ] || [ "$ff" = n ]
    do echo "
        ########################################################
        #                                                      #
        #  That is an invalid input. Please enter 'y' or 'n'.  #
        #  Please try again...                                 #
        #                                                      #
        ########################################################" && read -r ff
done

echo "
        ###############################################
        #                                             #
        #  Would you like to backup Thunderbird? y/n  #
        #                                             #
        ###############################################"
read -r tb

until [ "$tb" = y ] || [ "$tb" = n ]
    do echo "
        ########################################################
        #                                                      #
        #  That is an invalid input. Please enter 'y' or 'n'.  #
        #  Please try again...                                 #
        #                                                      #
        ########################################################" && read -r tb
done

echo "
        #################################################
        #                                               #
        #  Would you like to backup your Home directory #
        # sans Firefox and Thunderbird? y/n             #
        #                                               #
        #################################################"
read -r home

until [ "$home" = y ] || [ "$home" = n ]
    do echo "
        ########################################################
        #                                                      #
        #  That is an invalid input. Please enter 'y' or 'n'.  #
        #  Please try again...                                 #
        #                                                      #
        ########################################################" && read -r home
done

# Backup Firefox if required to do so by the user.
if [ "$ff" = 'y' ]
  then mv "$dest"/.mozilla "$dest"/.mozilla-"$(/bin/date +%Y-%m-%d--%H:%M:%S)" && rsync -av --progress "$source"/.mozilla "$destination"
    else echo "Firefox not being backed up."
fi

# Backup Thunderbird if required to do so by the user.
if [ "$tb" = 'y' ]
  then mv "$destination"/.thunderbird "$destination"/.thunderbird-"$(/bin/date +%Y-%m-%d--%H:%M:%S)" && rsync -av --progress "$source"/.thunderbird "$destination"
    else echo "Thunderbird not being backed up."
fi

# Backup home dir' if required to do so by the user. Note that the desktop is backed up differentially whereas the rest of the home dir' is backed up incrementally.
if [ "$home" = 'y' ]
  then rsync -av --progress --delete-after "$source"/Desktop/ "$destination"/Desktop && rsync -av --progress --delete "$source"/Downloads/ "$destination"/Downloads && rsync -av --progress --exclude 'Desktop' --exclude '.mozilla' --exclude '.thunderbird' --exclude 'Downloads' "$source"/ "$destination"
    else echo "Home not being backed up."
fi

# Let the user know we have finished!
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)

printf "
Total runtime: %d:%02d:%02d:%02.4f\n" "$dd" "$dh" "$dm" "$ds"
echo "
        #########################################
        #                                       #
        #   Finished!                           #
        #   Press enter to close this window.   #
        #                                       #
        #########################################"
read -r


