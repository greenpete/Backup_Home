## Backup Home
An rsync based script I use to backup my home directory in Linux.
It can back up just one, two or all of 'home', 'firefox', and 'thunderbird'. It will ask you each time you run it, what you would like to backup.

## Installation
After downloading the archive, decompress it and open the root directory and run the 'install.sh' file.
During installation, you will be asked for the source and destination. This is set one time at installation.

## Usage
You should find a shortcut in your menu under 'Other'.
Follow and answer the three questions about what you would like to back up.

## Uninstallation
Once you have installed this app, you will find its main files here ~/bin/backup_home. Inside you will find an uninstall.sh file. Run this and all files and menu entry will be removed.
